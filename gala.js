<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1295275/TweenMax.min.js"></script>
<script>
// Define an object that will be used to draw plus signs
var Plus = function() {
  this.x = 0;
  this.y = 0;

  this.top = 0;
  this.left = 0;

  this.height = 0;
  this.width = 0;
  this.scale = 1;
};

//Add draw method to the object
Plus.prototype.draw = function(ctx, x, y) {
  ctx.save();
  ctx.beginPath();
  ctx.setTransform(
    this.scale,
    0,
    0,
    this.scale,
    this.left + this.x,
    this.top + this.y
  );
  ctx.lineWidth = 2;

  ctx.moveTo(0, -this.height / 2);
  ctx.lineTo(0, this.height / 2);

  ctx.moveTo(-this.width / 2, 0);
  ctx.lineTo(this.width / 2, 0);

  ctx.stroke();
  ctx.closePath();
  ctx.restore();
};

var c = document.getElementById("c");
var context = c.getContext("2d");
var signs = [];
var mouse = { x: 0, y: 0 };
var gridLength = 9;
var mouseOver = false;
var mouseMoved = false;

c.width = window.innerWidth;
c.height = window.innerHeight;

// Create sign grid using 2D array
for (var i = 0; i < gridLength; i++) {
  signs[i] = [];
  for (var j = 0; j < gridLength; j++) {
    var min = Math.min(c.width, c.height);
    signs[i][j] = new Plus();
    signs[i][j].left = c.width / (gridLength + 1) * (i + 1);
    signs[i][j].top = c.height / (gridLength + 1) * (j + 1);
    signs[i][j].width = min / 100;
    signs[i][j].height = min / 100;
  }
}

// Use GSAP ticker to call draw function on every frame that will draw signs to the canvas
// You can use requestAnimationFrame as well
TweenLite.ticker.addEventListener("tick", draw);

function draw() {
  context.clearRect(0, 0, c.width, c.height);

  if (mouseOver && mouseMoved) {
    calculateSigns();
    mouseMoved = false;
  }

  for (var i = 0; i < gridLength; i++) {
    for (var j = 0; j < gridLength; j++) {
      var sign = signs[i][j];
      sign.draw(context);
    }
  }
}

function calculateSigns() {
  for (var i = 0; i < gridLength; i++) {
    for (var j = 0; j < gridLength; j++) {
      var sign = signs[i][j];
      var hyp = Math.min(c.width, c.height) / (gridLength + 1) / 2;
      var d = dist([sign.left, sign.top], [mouse.x, mouse.y]);
      var ax = mouse.x - sign.left;
      var ay = mouse.y - sign.top;
      var angle = Math.atan2(ay, ax);
      if (d < hyp + sign.width) {
        hyp = d;
        TweenMax.to(sign, 0.3, { scale: 2 });
      } else {
        TweenMax.to(sign, 0.3, { scale: 1 });
      }

      TweenMax.to(sign, 0.3, {
        x: Math.cos(angle) * hyp,
        y: Math.sin(angle) * hyp
      });
    }
  }
}

c.addEventListener("mousemove", mouseMove);
c.addEventListener("touchmove", mouseMove);

function mouseMove(e) {
  if (e.targetTouches && e.targetTouches[0]) {
    e = e.targetTouches[0];
  }
  var rect = c.getBoundingClientRect();
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
  mouseMoved = true;
}

c.addEventListener("mouseenter", function() {
  mouseOver = true;
});

c.addEventListener("touchstart", function(e) {
  mouseOver = true;
  mouseMove(e);
});

c.addEventListener("mouseleave", mouseLeave);
c.addEventListener("touchend", mouseLeave);

function mouseLeave() {
  mouseOver = false;

  for (var i = 0; i < gridLength; i++) {
    for (var j = 0; j < gridLength; j++) {
      var sign = signs[i][j];
      if (!mouseOver) {
        TweenMax.to(sign, 0.3, { x: 0, y: 0, scale: 1 });
      }
    }
  }
}

window.addEventListener("resize", function() {
  c.width = window.innerWidth;
  c.height = window.innerHeight;
  for (var i = 0; i < gridLength; i++) {
    for (var j = 0; j < gridLength; j++) {
      var min = Math.min(c.width, c.height);
      sign = signs[i][j];
      sign.left = c.width / (gridLength + 1) * (i + 1);
      sign.top = c.height / (gridLength + 1) * (j + 1);
      sign.width = min / 50;
      sign.height = min / 50;
    }
  }
});

function dist([x1, y1], [x2, y2]) {
  var dx = x1 - x2;
  var dy = y1 - y2;
  return Math.sqrt(dx * dx + dy * dy) || 1;
}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1295275/webgl-hover_min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1295275/TweenMax.min.js"></script>

<script>
new hoverEffect({
  parent: document.querySelector('.imageone'),
  intensity1: 0.1,
  intensity2: 0.1,
  angle2: Math.PI / 2,
  image1: 'https://uploads-ssl.webflow.com/5d1e0c59fdd1ef657d692c03/5d6d5849d05eb9f06645669c_1.jpg?',
  image2: 'https://uploads-ssl.webflow.com/5d1e0c59fdd1ef657d692c03/5d6d5849068031485597f1eb_2.jpg?',
  displacementImage: 'https://uploads-ssl.webflow.com/5b367b755b093e453cec2141/5bc9486cdc837c82711dfb73_displacement-4.png?'
});
</script>




<script>
new hoverEffect({
  parent: document.querySelector('.imagetwo'),
  intensity1: 0.1,
  intensity2: 0.1,
  angle2: Math.PI / 2,
  image1: 'https://uploads-ssl.webflow.com/5d1e0c59fdd1ef657d692c03/5d6d58cdd05eb9970f456945_2-1.jpg?',
  image2: 'https://uploads-ssl.webflow.com/5d1e0c59fdd1ef657d692c03/5d6d58cd0f8bc56a4afc7209_2-2.jpg?',
  displacementImage: 'https://uploads-ssl.webflow.com/5b367b755b093e453cec2141/5bc9486cdc837c82711dfb73_displacement-4.png?'
});
</script>



<script>
new hoverEffect({
  parent: document.querySelector('.imagefour'),
  intensity1: 0.1,
  intensity2: 0.1,
  angle2: Math.PI / 2,
  image1: 'https://uploads-ssl.webflow.com/5d1e0c59fdd1ef657d692c03/5d24d36d6ad06472f546af67_12.jpg?',
  image2: 'https://uploads-ssl.webflow.com/5d1e0c59fdd1ef657d692c03/5d24d36d6ad06472f546af67_12.jpg?',
  displacementImage: 'https://uploads-ssl.webflow.com/5b367b755b093e453cec2141/5bc9486cdc837c82711dfb73_displacement-4.png?'
});
</script>





<!-- BEGIN BLOTTER TEXT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Blotter/0.1.0/blotter.min.js"></script>
<script>
(function(Blotter) {
  Blotter.LiquidDistortMaterial = function() {
    Blotter.Material.apply(this, arguments);
  };

  Blotter.LiquidDistortMaterial.prototype = Object.create(
    Blotter.Material.prototype
  );

  Blotter._extendWithGettersSetters(
    Blotter.LiquidDistortMaterial.prototype,
    (function() {
      function _mainImageSrc() {
        var mainImageSrc = [
          Blotter.Assets.Shaders.Noise3D,

          "void mainImage( out vec4 mainImage, in vec2 fragCoord )",
          "{",
          "    // Setup ========================================================================",

          "    vec2 uv = fragCoord.xy / uResolution.xy;",
          "    float z = uSeed + uGlobalTime * uSpeed;",

          "    uv += snoise(vec3(uv, z)) * uVolatility;",

          "    mainImage = textTexture(uv);",

          "}"
        ].join("\n");

        return mainImageSrc;
      }

      return {
        constructor: Blotter.LiquidDistortMaterial,

        init: function() {
          this.mainImage = _mainImageSrc();
          this.uniforms = {
            uSpeed: { type: "1f", value: 1.0 },
            uVolatility: { type: "1f", value: 0.15 },
            uSeed: { type: "1f", value: 0.1 }
          };
        }
      };
    })()
  );
})(this.Blotter);

const body = document.body;
const docEl = document.documentElement;
const MathUtils = {
  lineEq: (y2, y1, x2, x1, currentVal) => {
    // y = mx + b
    var m = (y2 - y1) / (x2 - x1),
      b = y1 - m * x1;
    return m * currentVal + b;
  },
  lerp: (a, b, n) => (1 - n) * a + n * b,
  distance: (x1, x2, y1, y2) => {
    var a = x1 - x2;
    var b = y1 - y2;
    return Math.hypot(a, b);
  }
};

let winsize;
const calcWinsize = () =>
  (winsize = { width: window.innerWidth, height: window.innerHeight });

calcWinsize();
window.addEventListener("resize", calcWinsize);

const getMousePos = ev => {
  let posx = 0;
  let posy = 0;
  if (!ev) ev = window.event;
  if (ev.pageX || ev.pageY) {
    posx = ev.pageX;
    posy = ev.pageY;
  } else if (ev.clientX || ev.clientY) {
    posx = ev.clientX + body.scrollLeft + docEl.scrollLeft;
    posy = ev.clientY + body.scrollTop + docEl.scrollTop;
  }
  return { x: posx, y: posy };
};
let mousePos = { x: winsize.width / 2, y: winsize.height / 2 };
window.addEventListener("mousemove", ev => (mousePos = getMousePos(ev)));
const elem = document.getElementById("blotter-wrapper");
const textEl = document.getElementById("blotter");
const createBlotterText = () => {
  const text = new Blotter.Text(textEl.innerHTML, {
    family: "Montserrat",
    weight: 400,
    size: 20,
    fill: "#ffffff",
    paddingLeft: 40
  });
  elem.removeChild(textEl);

  const material = new Blotter.LiquidDistortMaterial();
  material.uniforms.uSpeed.value = 1;
  material.uniforms.uVolatility.value = 0;
  material.uniforms.uSeed.value = 0.1;
  const blotter = new Blotter(material, { texts: text });
  const scope = blotter.forText(text);
  scope.appendTo(elem);
  let lastMousePosition = { x: winsize.width / 2, y: winsize.height / 2 };
  let volatility = 0;
  const render = () => {
    const docScrolls = {
      left: body.scrollLeft + docEl.scrollLeft,
      top: body.scrollTop + docEl.scrollTop
    };
    const relmousepos = {
      x: mousePos.x - docScrolls.left,
      y: mousePos.y - docScrolls.top
    };
    const mouseDistance = MathUtils.distance(
      lastMousePosition.x,
      relmousepos.x,
      lastMousePosition.y,
      relmousepos.y
    );
    volatility = MathUtils.lerp(
      volatility,
      Math.min(MathUtils.lineEq(0.9, 0, 100, 0, mouseDistance), 0.9),
      0.05
    );
    material.uniforms.uVolatility.value = volatility;
    lastMousePosition = { x: relmousepos.x, y: relmousepos.y };
    requestAnimationFrame(render);
  };
  requestAnimationFrame(render);
};
createBlotterText();
</script>
<!-- END BLOTTER TEXT -->


